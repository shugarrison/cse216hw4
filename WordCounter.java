import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.TreeMap;

public class WordCounter {
        // The following are the ONLY variables we will modify for grading.
        // The rest of your code must run with no changes.
        public static final Path FOLDER_OF_TEXT_FILES  = Paths.get("C:\\Users\\super\\Desktop\\Java Programs\\CSE216HW4\\TextFiles"); // path to the folder where input text files are located
        public static final Path WORD_COUNT_TABLE_FILE = Paths.get("C:\\Users\\super\\Desktop\\Java Programs\\CSE216HW4\\output.txt"); // path to the output plain-text (.txt) file
        public static final int  NUMBER_OF_THREADS     = 1;                // max. number of threads to spawn 
        private static TreeMap<String, Integer> total = new TreeMap<String, Integer>();
    	private static List<TreeMap<String, Integer>> textMaps = new ArrayList<TreeMap<String, Integer>>();
    	
    	public static class fileReader implements Runnable {
    		private File file;
    		
    		public fileReader(File f) {
    			this.file = f;
    		}
    		
    		public void run() {
    			try {
    				textMaps.add(textToMap(file));
    			} catch (FileNotFoundException e) {
    				System.out.println("File not found");
    			}
    		}
    	}
        
        public static TreeMap<String, Integer> textToMap(File text) throws FileNotFoundException {
        	Scanner file = new Scanner(text);
        	TreeMap<String, Integer> map = new TreeMap<String, Integer>();
        	while(file.hasNext()) {
        		String word = file.next();
        		word = word.replaceAll("\\p{Punct}", "");
        		word = word.toLowerCase();
        		if (map.containsKey(word)) {
        			map.put(word, map.get(word) + 1);
        		}
        		else {
        			map.put(word, 1);
        		}
        		if (total.containsKey(word)) {
        			total.put(word, map.get(word) + 1);
        		}
        		else {
        			total.put(word, 1);
        		}
        	}
        	file.close();
        	return map;
        }
        
        private static File[] folderToArray(Path path) {
        	File folder = path.toFile();
        	return folder.listFiles();
        }
        
        private static void printSpace(FileWriter file, int n) throws IOException {
        	for (int i = 0; i < n; i++) {
        		file.write(" ");
        	}
        }
        
        public static void main(String... args) throws IOException {
            // your implementation of how to run the WordCounter as a stand-alone multi-threaded program
        	int threads = NUMBER_OF_THREADS;
        	int column_size = 0;
        	int column1_size = 0;
        	int index = 0;
        	Thread[] threadArray = new Thread[threads];
        	File[] fileArray = folderToArray(FOLDER_OF_TEXT_FILES);
        	FileWriter output = new FileWriter(WORD_COUNT_TABLE_FILE.toFile());
        	Arrays.sort(fileArray);
        	long start = System.currentTimeMillis();
        	//MultiThreadingPortion
        	for (File f : fileArray) {
        		if (threads <= 1) {
        			try {
        				textMaps.add(textToMap(f));
        				if (f.getName().length() > column_size) { //finds column size using length of longest file name
        					column_size = f.getName().length();
        				}
        			} catch (FileNotFoundException e) {
        				System.out.println("File not found");
        			}
        		}
        		else {
        			threads--; //decrement number of threads
        			threadArray[index] = new Thread(new fileReader(f));
        			threadArray[index].start();
        			index++;
        			if (f.getName().length() > column_size) {
    					column_size = f.getName().length();
        			}
        		}
        	}
        	for (int i = 0; i < index; i++) {
        		try{
        			threadArray[i].join();
        		}
        		catch(InterruptedException e) {}
        	}
        	long end = System.currentTimeMillis();
        	for (String str: total.keySet()) { //finds size of first column based on length of longest word
        		if (str.length() > column1_size) {
        			column1_size = str.length();
        		}
        	}
        	column_size++;
        	column1_size++;
        	printSpace(output, column1_size); //skip first column first row
        	for (File f : fileArray) {
        		output.write(f.getName()); //print text file names
        		printSpace(output, column_size - f.getName().length()); //print space to next column
        	}
        	output.write("total");
        	output.write("\n"); //next row
        	
        	for (String str: total.keySet()) { //iterate through words
        		output.write(str); //print word
        		printSpace(output, column1_size - str.length());
        		for (TreeMap<String, Integer> map : textMaps) { //iterate through text files
        			if (map.containsKey(str)) { //if text file contains word
        				output.write(map.get(str).toString());
        				printSpace(output, column_size - String.valueOf(map.get(str)).length()); //print spaces till next column
        			}
        			else {
        				output.write('0');
        				printSpace(output,column_size - 1);
        			}
        		}
        		output.write(total.get(str).toString());
        		
        		output.write("\n"); //next row
        	}
        	output.close();
        	System.out.print(end - start);
        }
    }
