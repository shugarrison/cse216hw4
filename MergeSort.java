import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class MergeSort {
	private static final Random RNG    = new Random(10982755L);
    private static final int    LENGTH = 524288;

    public static void main(String... args) {
        int[] arr = randomIntArray();
        long start = System.currentTimeMillis();
        concurrentMergeSort(arr);
        long end = System.currentTimeMillis();
        if (!sorted(arr)) {
            System.err.println("The final array is not sorted");
            System.exit(0);
        }
        System.out.printf("%10d numbers: %6d ms%n", LENGTH, end - start);
    }

    private static int[] randomIntArray() {
        int[] arr = new int[LENGTH];
        for (int i = 0; i < arr.length; i++)
            arr[i] = RNG.nextInt(LENGTH * 10);
        return arr;
    }

    public static boolean sorted(int[] arr) {
		for (int i = 0; i < arr.length - 1; i++) {
			if (arr[i] > arr[i + 1]) {
				return false;
			}
		}
		return true;
	}
    
    public static void concurrentMergeSort(int[] arr) {
    	int cores = Runtime.getRuntime().availableProcessors();
    	concurrentMergeSort(arr, cores);
    }
    
    public static void concurrentMergeSort(int[] arr, int threads) {
    	//if thread limit reached, normal single thread sort
    	if (threads <= 1) {
    		mergeSort(arr);
    	}
    	else if(arr.length >= 2) {
    		//divide into two arrays of half size
    		int[] left = Arrays.copyOfRange(arr, 0, arr.length/2);
    		int[] right = Arrays.copyOfRange(arr, arr.length/2, arr.length);
    		
    		//divide into two threads
    		Thread leftThread = new Thread(new Sorting(left, threads/2));
    		Thread rightThread = new Thread(new Sorting(right, threads/2));
    		
    		//concurrentMergeSort(left)
    		//concurrentMergeSort(right)
    		leftThread.start();
    		rightThread.start();
    		
    		try {
    			leftThread.join();
    			rightThread.join();
    		}
    		catch (InterruptedException e) {}
    		
    		//merge arrays
    		merge(left, right, arr);
    	}
    }
    
    public static void mergeSort(int[] arr) {
		if (arr.length >= 2) {
			// split array in half
			int[] left  = Arrays.copyOfRange(arr, 0, arr.length / 2);
			int[] right = Arrays.copyOfRange(arr, arr.length / 2, arr.length);
			
			// sort the half arrays
			mergeSort(left);
			mergeSort(right);
			
			// merge arrays
			merge(left, right, arr);
		}
	}
    
    public static void merge(int[] left, int[] right, int[] arr) {
		int i1 = 0;
		int i2 = 0;
		//for each element in arr, append the lower of the two values
		for (int i = 0; i < arr.length; i++) {
			if (i2 >= right.length || (i1 < left.length && left[i1] < right[i2])) {
				arr[i] = left[i1];
				i1++;
			} else {
				arr[i] = right[i2];
				i2++;
			}
		}
	}
    
}
