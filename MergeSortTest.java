import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MergeSortTest {

	@Test
	void sortedMethodTest() {
		int[] arr = new int[]{1,2,3,4,5};
		int[] arr1 = new int[0];
		int[] arr2 = new int[10];
		int[] arr3 = new int[] {-1,2,0,4};
		int[] arr4 = new int[] {0,0,0,0,-1};
		int[] arr5 = new int[] {0,0,0,0,0};
		Assertions.assertAll(
				() -> Assertions.assertTrue(MergeSort.sorted(arr)),
				() -> Assertions.assertTrue(MergeSort.sorted(arr1)),
				() -> Assertions.assertTrue(MergeSort.sorted(arr2)),
				() -> Assertions.assertTrue(MergeSort.sorted(arr3)),
				() -> Assertions.assertTrue(MergeSort.sorted(arr4)),
				() -> Assertions.assertTrue(MergeSort.sorted(arr5))
				);
	}

}
